<?php

namespace VolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use VolBundle\Entity\Vol;
use VolBundle\Form\VolType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function allVolAction() {

        $repository = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('VolBundle:Vol');
        $fligths = $repository->findAll();

        return $this->render('VolBundle:Default:index.html.twig', array(
            'vols' => $fligths,
        ));
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function allVolAdminAction() {

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('VolBundle:Vol');
        $fligths = $repository->findAll();

        return $this->render('VolBundle:Default:admin.html.twig', array(
            'vols' => $fligths,
        ));
    }

    /**
     * @Route("/create", name="create")
     */
    public function createVolAction(Request $request) {

        $repo = $this->getDoctrine()->getManager();
        $form = $this->createForm(VolType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $repo->persist($task);
            $repo->flush();
            $this->addFlash('success', "Le vol a été crée");

            return $this->redirectToRoute('home');
        }

        return $this->render('VolBundle:Default:create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function deleteVolAction($id) {
        $vol = $this->getDoctrine()
            ->getManager()
            ->getRepository('VolBundle:Vol')
            ->find($id);

        $this->getDoctrine()
                ->getManager()
                ->remove($vol);

        $this->getDoctrine()
            ->getManager()
            ->flush();

        $this->addFlash('success', 'le vol a été supprimée');

        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function editVolAction(Request $request, $id){

        $vol = $this->getDoctrine()
            ->getManager()
            ->getRepository('VolBundle:Vol')
            ->find($id);
        $form = $this->createForm(VolType::class, $vol);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Modification du vol validé');
        }

        return $this->render('VolBundle:Default:edit.html.twig',['form' => $form->createView()]);
    }
}
