<?php

namespace VolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vol
 *
 * @ORM\Table(name="vol")
 * @ORM\Entity(repositoryClass="VolBundle\Repository\VolRepository")
 */
class Vol
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_vol", type="string", length=255, unique=true)
     */
    private $numeroVol;

    /**
     * @var string
     *
     * @ORM\Column(name="depart", type="string", length=255)
     */
    private $depart;

    /**
     * @var string
     *
     * @ORM\Column(name="arrivee", type="string", length=255)
     */
    private $arrivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horaire", type="datetime")
     */
    private $horaire;

    /**
     * @var float
     *
     * @ORM\Column(name="places", type="float")
     */
    private $places;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var bool
     *
     * @ORM\Column(name="promo", type="boolean")
     */
    private $promo;

    /**
     * @var bool
     *
     * @ORM\Column(name="complet", type="boolean")
     */
    private $complet;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroVol
     *
     * @param string $numeroVol
     *
     * @return Vol
     */
    public function setNumeroVol($numeroVol)
    {
        $this->numeroVol = $numeroVol;

        return $this;
    }

    /**
     * Get numeroVol
     *
     * @return string
     */
    public function getNumeroVol()
    {
        return $this->numeroVol;
    }

    /**
     * Set depart
     *
     * @param string $depart
     *
     * @return Vol
     */
    public function setDepart($depart)
    {
        $this->depart = $depart;

        return $this;
    }

    /**
     * Get depart
     *
     * @return string
     */
    public function getDepart()
    {
        return $this->depart;
    }

    /**
     * Set arrivee
     *
     * @param string $arrivee
     *
     * @return Vol
     */
    public function setArrivee($arrivee)
    {
        $this->arrivee = $arrivee;

        return $this;
    }

    /**
     * Get arrivee
     *
     * @return string
     */
    public function getArrivee()
    {
        return $this->arrivee;
    }

    /**
     * Set horaire
     *
     * @param \DateTime $horaire
     *
     * @return Vol
     */
    public function setHoraire($horaire)
    {
        $this->horaire = $horaire;

        return $this;
    }

    /**
     * Get horaire
     *
     * @return \DateTime
     */
    public function getHoraire()
    {
        return $this->horaire;
    }

    /**
     * Set places
     *
     * @param float $places
     *
     * @return Vol
     */
    public function setPlaces($places)
    {
        $this->places = $places;

        return $this;
    }

    /**
     * Get places
     *
     * @return float
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Vol
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set promo
     *
     * @param boolean $promo
     *
     * @return Vol
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return bool
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set complet
     *
     * @param boolean $complet
     *
     * @return Vol
     */
    public function setComplet($complet)
    {
        $this->complet = $complet;

        return $this;
    }

    /**
     * Get complet
     *
     * @return bool
     */
    public function getComplet()
    {
        return $this->complet;
    }
}

